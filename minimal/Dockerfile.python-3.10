FROM jupyter/minimal-notebook:lab-3.1.18

ARG conda_env=python310
ARG py_ver=3.10
ARG disp_name="Python 3.10 (ipykernel)"

USER root

RUN echo "${conda_env}" > /opt/custom_jupyter_kernel

# Install all OS dependencies for fully functional notebook server
RUN apt-get update --yes && \
    apt-get install --yes --no-install-recommends \
    vim-tiny \
    git \
	curl \
    inkscape \
    libsm6 \
    libxext-dev \
    libxrender1 \
    lmodern \
    netcat \
    openssh-client \
    # ---- nbconvert dependencies ----
    texlive-xetex \
    texlive-fonts-recommended \
    texlive-plain-generic \
    # ----
    tzdata \
    unzip \
    nano-tiny && \
    apt-get clean && rm -rf /var/lib/apt/lists/*

# Create alternative for nano -> nano-tiny
RUN update-alternatives --install /usr/bin/nano nano /bin/nano-tiny 10

RUN apt-get update && \
	apt install -y git

# Install Git-LFS
RUN curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | sudo bash

RUN apt-get update && \
	apt-get install -y git-lfs

USER ${NB_USER}

# Fix setuptools pip package
RUN pip uninstall -y setuptools
RUN pip install setuptools==75.8.1
RUN pip install setuptools==61.0.0
RUN pip install backports.tarfile

RUN mamba create --yes -p "${CONDA_DIR}/envs/${conda_env}" python=${py_ver} ipython ipykernel && \
	mamba clean --all -f -y

# create Python kernel and link it to conda base jupyter kernels
RUN "${CONDA_DIR}/envs/${conda_env}/bin/python" -m ipykernel install --prefix=${CONDA_DIR} --name="${conda_env}" --display-name "${disp_name}" && \
    fix-permissions "${CONDA_DIR}" && \
    fix-permissions "/home/${NB_USER}"

RUN echo -n '{\n "argv": [\n  "/opt/conda/envs/CONDA_ENV/bin/python",\n  "-m",\n  "ipykernel_launcher",\n  "-f",\n  "{connection_file}"\n ],\n "display_name": "DISP_NAME",\n "language": "python",\n "metadata": {\n  "debugger": true\n }\n}\n' | sed "s/CONDA_ENV/$conda_env/g" | sed "s/DISP_NAME/$disp_name/g" | awk '{gsub(/\\n/,"\n")}1' > /opt/conda/share/jupyter/kernels/python3/kernel.json

RUN rm /opt/conda/share/jupyter/kernels/${conda_env}/kernel.json

RUN pip install \
	'jupyterlab_iframe==0.4.0' \
	'git+https://github.com/stv0g/nbgitpuller@f735265f7b2a429a17a8fab70cfd3557f060640d' \
	'rwth-nb==0.1.8' \
	'tqdm'

RUN conda install --name base --no-update-deps \
		'conda==4.10.3' && \
	conda install --name base --quiet --yes --no-update-deps \
		'jupyterlab-git==0.32.1' \
		'ujson==4.0.2' \
		'ipympl==0.9.3' \
		'matplotlib-base==3.4.3' && \
	conda clean --all


RUN conda install --name base --quiet --yes --no-update-deps \
		'jupyterlab-drawio==0.9.0' \
		'jupyterlab_iframe==0.4.0' \
		'jupyterlab_latex=3.0.0' \
		'jupyter_core==4.11.2' && \
	conda clean --all

RUN conda install --name base --quiet --yes --no-update-deps --channel conda-forge \
                'jupyterlab_widgets' \
		'jupyterhub=1.1.0' \
		'ipywidgets' && \
        conda clean --all

# Fix traitlets pip package
RUN pip uninstall -y traitlets
RUN pip install traitlets==5.9.0

# Workaround LaTex
RUN git clone https://github.com/joequant/jupyterlab-latex /tmp/jupyterlab-latex && cd /tmp/jupyterlab-latex && git checkout cbb66825786ecf11a35fd92df797f8ccee719ad4 && pip install -ve .
RUN jupyter lab build


RUN jlpm cache clean
RUN pip install jupyterlab-topbar
# Install requirements for textbox building
RUN conda install --name base -c conda-forge nodejs=16
RUN npm install --global typescript@4.2.4
# Install TextBox Repository
RUN git clone https://gitlab+deploy-token-1194:gldt-qyCGuGcefqc6YBxGirqd@git.rwth-aachen.de/jupyter/jupyterlab-display-hub-profile /tmp/jupyterlab-display-hub-profile && cd /tmp/jupyterlab-display-hub-profile && jlpm && jlpm build:lib:prod && tsc && jupyter labextension install .
ADD overrides.json /opt/conda/share/jupyter/lab/settings/overrides.json

RUN jupyter lab build

USER root

# Use RWTH Mirror
#RUN sed -i 's|http://archive.ubuntu.com|http://ftp.halifax.rwth-aachen.de|g' /etc/apt/sources.list

RUN apt-get update && \
	apt-get -y install \
		language-pack-de \
		texlive-latex-recommended \
		openssh-client && \
	rm -rf /var/lib/apt/lists/*

# Add more locales
RUN locale-gen de_DE && \
	locale-gen de_DE.UTF-8 && \
	update-locale

USER ${NB_USER}

ENV JUPYTER_ENABLE_LAB=yes
