#!/bin/bash

CLUSTER_LOGIN_NODE=login18-1.hpc.itc.rwth-aachen.de
DIR=~/.jupyter-rwth-hpc

if [ -f "${DIR}/settings.sh" ]; then
	. ${DIR}/settings.sh
fi

# Run on cluster
CRUN="ssh -l ${CLUSTER_USER} ${CLUSTER_LOGIN_NODE} --"

# Unique ID for this Jupyter spawn.
# Required for supporting multiple Jupyter jobs in SLURM
SPAWN_ID=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 8 | head -n 1)

# set a random port for the notebook, in case multiple notebooks are
# on the same compute node.
PORT=$(shuf -i 8000-8500 -n 1)

function finish {
	# Close tunnel
	if [ -n "${SSH_PID}" ]; then
		kill ${SSH_PID}
	fi

	# Cancel job
	if [ -n $${JOB_ID} ]; then
		${CRUN} scancel ${JOB_ID}
	fi
}
trap finish EXIT

# Queue job
${CRUN} /bin/bash sbatch <<EOF
#!/bin/bash

#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --mem-per-cpu=8G
#SBATCH --time=1-0:00:00
#SBATCH --job-name=jupyter-${SPAWN_ID}
#SBATCH --output=jupyter-notebook-%J.log
#SBATCH --partition=c16m

module switch intel gcc
module load python/3.6.0

python3 -m pip install --user --upgrade pip
python3 -m pip install --user --upgrade jupyterhub jupyterlab

srun -n1 \$(python3.6 -m site --user-base)/bin/jupyterhub-singleuser --no-browser --ip=0.0.0.0 --port=${PORT}
EOF

# Wait until job is scheduled
JOB_STATE="UNKNOWN"
while [ "${JOB_STATE}" != "RUNNING" ]; do
	# Check job status
	JOB_STATUS=$(${CRUN} squeue -u ${CLUSTER_USER} -h -o \"%.i %j %T %N %R\" | grep ${SPAWN_ID})
	if [ -z "${JOB_STATUS}" ]; then
		break;
	fi

	read -r JOB_ID JOB_NAME JOB_STATE JOB_NODE JOB_REASON <<<${JOB_STATUS}
	echo "Waiting for job ${JOB_ID} (${JOB_NAME}) on node ${JOB_NODE} to run. Current state: ${JOB_STATE} (${JOB_REASON})"
	sleep 1
done

# Setup tunnel
ssh -L"[::]:8888:${JOB_NODE}:${PORT}" -l ${CLUSTER_USER} ${CLUSTER_LOGIN_NODE} -N &
SSH_PID=$!

echo "Jupyter started.."
echo
echo "   Access at: http://localhost:8888"

echo "Sleeping. Waiting for termination"
while true; do
	sleep inf &
	wait $!
	echo "Sleep over.." # shouldnt happen?
done
