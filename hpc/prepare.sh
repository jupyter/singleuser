#!/bin/bash

CLUSTER_LOGIN_NODE=login18-1.hpc.itc.rwth-aachen.de
DIR=~/.jupyter-rwth-hpc

read -p "HPC Username: " CLUSTER_USER
read -p "HPC Password: " CLUSTER_PASSWORD

#CLUSTER_PASSWORD=$(pass rwth/hpc | head -n1)

mkdir -p ${DIR}

ssh-keygen \
    -C ${USER}@jupyter.rwth-aachen.de \
    -q -t rsa -b 4096 -N ""\
    -f ${DIR}/id_rsa

sshpass -p ${CLUSTER_PASSWORD} \ 
ssh-copy-id \
    -i ${DIR}/id_rsa \
    ${CLUSTER_USER}@${CLUSTER_LOGIN_NODE}

cat << <<EOF > ~/.jupyter-rwth-hpc/settings.sh
CLUSTER_USER=${CLUSTER_USER}
EOF
